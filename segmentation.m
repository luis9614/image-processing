f = im2double(imread('data/S7-Segmentation/Patt2.jpg'));
figure;
imshow(f);

%mask = [-1 -1 -1; -1 8 -1; -1 -1 -1]; % Laplacian
%mask = [-1 -1 -1; 2 2 2; -1 -1 -1]; % Horizontal
%mask = [-1 2 -1; -1 2 -1; -1 2 -1]; % Vertical
mask = [-1 -1 2; -1 2 -1; 2 -1 -1]; % Diagonal 45°
R = abs(imfilter(f, mask)); 
figure;
imshow(R);
% R>=T

T = max(R(:)); % Threshold is the max value in R
% for T= 0.1:0.1:1.4
%     U = R>=T;
%     imshow(U);
%     pause(0.6);
% end
figure;
U = R >= T; 
imshow(U);


% Border Detection
f = im2double(imread('data/S7-Segmentation/Patt3.tif'));
maskx = [-1 -2 -1;0 0 0; 1 2 1];
Gx = abs(imfilter(f, maskx));

masky = [-1 0 1; -2 0 2; -1 0 1];
Gy = abs(imfilter(f, masky));

G = Gx + Gy;


subplot(1,2,1);
imshow(f);
subplot(1,2,2);
imshow(G);

% Canny Filter + edge()
f = im2double(imread('data/S7-Segmentation/Patt3.tif'));
[g, t] = edge(f, 'canny', 'horizontal'); % Supports all masks mentioned before
figure;
imshow(g);

% hough
A = zeros(100,100);
A(1,1) = 1;
A(50,50) = 1;
A(1,100) = 1;
A(100,1) = 1;
A(100,100) = 1;
subplot(1,2,1);
imshow(A);

% Hough Transform
subplot(1,2,2);
[H, theta, rho] = hough(A);
imagesc(H);
imshow(imadjust(rescale(H)),'XData',theta,'YData',rho,'InitialMagnification','fit');
title('Hough Transform');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;
colormap(gca,hot);