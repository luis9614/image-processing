A = zeros(100,100);
A(1,1) = 1;
A(50,50) = 1;
A(1,100) = 1;
A(100,1) = 1;
A(100,100) = 1;
subplot(1,2,1);
imshow(A);

% Hough Transform
subplot(1,2,2);
[H, theta, rho] = hough(A);
imagesc(H);
imshow(imadjust(rescale(H)),'XData',theta,'YData',rho,'InitialMagnification','fit');
title('Hough Transform');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;
colormap(gca,hot);