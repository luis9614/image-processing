# Utilities

Snippets for faster coding.

## Get Noise for an Image

```Matlab
addpath('/func/histroi.m');
IMAGE_PATH='S4-Restauration/noisy_image2.tif';

f = imread(IMAGE_PATH);
imshow(f);
[ B, c, r ] = roipoly(f); % Region Of Interest - draws poligont in free movement
imshow(B); % B = Region contained by the polygon
[ p, ~ ] = histroi(f, c, r); % generate region's histogram
figure, bar(p);
```

**Note:** use only on regular/uniform regions, as they provide more accurate noise representations to the histogram.  

## Get noise filter

Allows for fast filter creation.  

```Matlab
% Dependencies
addpath('/func/paddedsize.m');
addpath('/func/lpfilter.m');
addpath('/func/dftuv.m');
addpath('/util/get_filter_luis.m');

% Actual Function
FILTER = 'BP'; % Could be BP, BR, HP or LP
H = get_filter_luis(FILTER, 'ideal', PQ(1), PQ(2), 100, 200);
```

## imnoise2

Allows for fast noise-matrix creation.  

```Matlab
% Dependencies
addpath('/func/imnoise2.m');
addpath('/func/imnoise3.m');
addpath('/util/auto_adjust.m');

% Actual Function
n = imnoise2('gaussian', M, N, 0.5, 0.1);
n = imnoise2('rayleigh', M, N, 0, 1);
n = imnoise2('erlang', M, N, 7, 2);
n = imnoise2('exponential', M, N, 5);
n = imnoise2('uniform', M, N, 0.25, 0.75);
n = imnoise2('salt & pepper', M, N, 0.5, 0.5);
```

## Add every subfolder to Path

```Matlab
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

rmpath('/Users/luiscorrea/Matlab/S4-Restauration/'); % Removes specific directories
```
