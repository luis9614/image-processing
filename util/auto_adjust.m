function [na] = auto_adjust(n)
    na = (n-min(n(:)))/(max(n(:))-min(n(:)));
end