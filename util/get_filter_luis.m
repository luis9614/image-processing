function H = get_filter_luis( pass_func, filter_type, M, N, varargin )
    % H = get_filter('LP', 'ideal', 500, 500, 100);
    % H = get_filter('LP', 'ideal', 500, 500, 100, 200);

    var1 = varargin{1};

    if nargin==5
        disp('I got here!');
        switch pass_func
        case 'LP'
            disp('And here!');
            H = lpfilter( filter_type, M, N, var1); 
        case 'HP'
            H = 1 - lpfilter( filter_type, M, N, var1);
        otherwise
            disp('Not a valid function (use LP or HP) or incorrect number of parameters.');
        end
    elseif nargin==6
        var2 = varargin{2};
        switch pass_func
        case 'BP'
            H = lpfilter( filter_type, M, N, var1) + (1 - lpfilter( filter_type, M, N, var2));
        case 'BR'
            H = 1 - (lpfilter( filter_type, M, N, var1) + (1 - lpfilter( filter_type, M, N, var2)));
        otherwise
            disp('Not a valid function (use BP or BR) or incorrect number of parameters.');
        end
    end
end


% Filter - Class Comments
    % Last parameter is half fc (cut frequency)
    %H = lpfilter('ideal', PQ(1), PQ(2), 100); 
    %H = get_filter_luis('BP', 'ideal', PQ(1), PQ(2), 100, 200);
    %H = 1- lpfilter('ideal', PQ(1), PQ(2), 100); %High pass filter
    
    % Band Reject (HighPass + LowPass)
    %H1 = lpfilter('ideal', PQ(1), PQ(2), 10); 
    %H2 = 1- lpfilter('ideal', PQ(1), PQ(2), 20); %High pass filter
    %H = H1 + H2;
    
    % Band Pass
    % 1-BR
    %H1 = lpfilter('ideal', PQ(1), PQ(2), 260); 
    %H2 = 1- lpfilter('ideal', PQ(1), PQ(2), 500); %High pass filter
    %H = 1 - (H1 + H2);
    
    %G = F.*H;