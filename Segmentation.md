# Segmentation

## Description

File: [segmentation.m](segmentation.m)  
Contributors: Luis Correa  

October, 2018  

## Contents

* [Introduction](#Introduction)  

## Introduction

Dividing images into regions or objects, based on discontinuities or similarities.  

```Matlab
f = im2double(imread('data/S7-Segmentation/Patt1.jpg'));
imshow(f);

mask = [-1 -1 -1; -1 8 -1; -1 -1 -1];
R = abs(imfilter(f, mask));
figure;
imshow(R);
% R>=T

T = max(R(:)); % Threshold is the max value in R
figure;
U = R >= T;
imshow(U);
```

### Filters for Line Detection

```Matlab
mask = [-1 -1 -1; -1 8 -1; -1 -1 -1]; % Laplacian
mask = [-1 -1 -1; 2 2 2; -1 -1 -1]; % Horizontal
mask = [-1 2 -1; -1 2 -1; -1 2 -1]; % Vertical
mask = [-1 -1 2; -1 2 -1; 2 -1 -1]; % Diagonal 45°
```

## Border Detection

There are three availiable border detecting masks, Sobel, Roberts and Porwin.  
![Masks](images/border.png)

![Exercise](images/BordersEx1.png)

### Canny Mask

Includes  

* Gaussian Filter for noise detection  
* Calculates the gradient and direction  
* Threshold for eliminating weak pixels  
* Retains the pixels in 8-A  

### edge()

Requires grayscale image, last parameter is optional.

```Matlab
f = im2double(imread('data/S7-Segmentation/Patt3.tif'));
[g, t] = edge(f, 'canny', 'horizontal'); % Supports all masks mentioned before
```

## Hough

Describes colineal point in an image.  
Every point is described in polar coordinates, and graphed as a sum of a sine and cosine function, allowing to graphically express colineal points.

![Hough](images/Hough.png)

Every intersection represents all the colineal points in an image.  

```Matlab
% Dummy Image, five points
A = zeros(100,100);
A(1,1) = 1;
A(50,50) = 1;
A(1,100) = 1;
A(100,1) = 1;
A(100,100) = 1;

% Hough Transform
[H, theta, rho] = hough(A);
imagesc(H);
imshow(imadjust(rescale(H)),'XData',theta,'YData',rho,'InitialMagnification','fit');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;
colormap(gca,hot);
```