# Morphology

## Description

File: [morphology.m](morphology.m)  
Contributors: Luis Correa  

October, 2018  

## Contents

* [Image Filling](#Image-Filling)  
* [Labelling](#Connected-Elements-(Labelling))  
* [Morphological Reconstruction](#Morphological-Reconstruction)  
* [Non-Binary Reconstruction](#Non-Binary-Reconstruction)  
* [Top Hat](#Top-Hat)  
* [Binarization](#Binarization)  
* [Exercises](#Exercises)  

## Image Filling

![Region Filling](images/RegionFilling.png)

Filling an image requires having a structural element suitable for filling a countoured area, a set starting point and an auxiliary image that will contain the iterated modifications.  

```Matlab
% Image A
A = [   0 0 0 0 0 0 0;
        0 0 1 1 0 0 0;
        0 1 0 0 1 0 0;
        0 0 1 0 1 0 0;
        0 0 1 0 1 0 0;
        0 1 0 0 0 1 0;
        0 1 0 0 0 1 0;
        0 1 1 1 1 0 0;
        0 0 0 0 0 0 0;
];

% Structural Element
B = strel('diamond', 1);

% The image that will be modified on each step
Xk = zeros(size(img));

% Starting Point
Xk(3, 3) = 1;

% Algorithm
flag = 0;
while flag == 0
    subplot(1, 2, 2);
    imagesc(or(Xn, A));
    pause(0.4);
    Xn = imdilate(Xk, B)&~A;
    if(Xn-Xk)==0
        flag=1;
    else
        Xk=Xn;
    end
end
```

## Connected Elements (Labelling)

Suitable for binary images. It only works with connectivity 4 and 8.  
![Labelling Original](images/LabellingOriginal.png)
![Labelling Result](images/Labelling.png)

```Matlab
f = imread('data/S6-Morphology/five-objects.tif');
imagesc(f);

[L, num] = bwlabel(f, 8); % [Labels, Number of Objects] = bwlabel(file, connectivity)

% Prints all objects
fig = 1:1:num;
for i=fig
    subplot(1, num, i);
    imagesc(L==i);
    colormap('gray');
end
```

## Morphological Reconstruction

### Theory

Dilate until only points are left, those that contain the desired features.
![Morphological Reconstruction](images/Reconstruction.jpg)

### Code

```Matlab
% Objective, to keep the letters that have a 'palito'
% Choose a structural pattern that fits the features you want to keep. In this case, a line.
A = imread('data/S6-Morphology/book-text.tif');

B = strel('line', 50, 90); % Structural Element containing the desired feature, a vertical rectangle.
figure;
C = imerode(A, B); % Erode image. Only points are left.

R = imreconstruct(C, A); % Reconstructed image containing the desired feature.
```

## Non-Binary Reconstruction

![Non Binary Reconstruction](images/NonBinaryReconstruction.jpg)

```Matlab
A = imread('data/S6-Morphology/aerial.tif');
B = strel('square', 5);
C = imdilate(A, B);
D = imerode(A, B);

E = C - D; % Dilation - Erosion returns contours
figure;
imshow(E);
title('Dilation-Erosion=Contours')
```

## Top Hat

Generates a uniform image, eliminating differences in lighning in a scene.

![TopHat](images/TopHat.jpg)

```Matlab
A = imread('data/S6-Morphology/rice.tif');
B = strel('disk', 10);
C = imtophat(A, B);
```

## Binarization

Uses the image's histogram to locate an inflection point and divide the image's value into 0 and 1.  

![Binarization](images/Binarization.png)

```Matlab
A = imread('data/S6-Morphology/aerial.tif');
T = graythresh(A);
C = imadjust(A, [T, T+1e-4], [0, 1]);
```

Result Below
![Binarization](images/Binarized.jpg)

## Exercises

### Calculator

> Objective: to extract the calculator's key's text, ignoring any other feature from the image.  

![Binarization](images/Calculator.jpg)

```Matlab
A = imread('data/S6-Morphology/calculator.tif');
% Binarizing
T = graythresh(A);
C = imadjust(A, [T, T+1e-4], [0, 1]);

% Structural Element for removing horizontal keyboard lines.
B = strel('line', 35, 0);
D = imerode(C, B); % Get horizontal lines by eroding.
R = imreconstruct(D, C); % Reconstruct horizontal lines.
RES = C - R; % Remove horizontal lines from original.

% Structural Element for removing vertical keyboard lines.
B = strel('line', 4, 0);
D = imerode(RES, B); % Get everything but vertical lines
R = imreconstruct(D, RES); % Reconstruct image
figure;
imshow(R);
```