f = imread('S5-Color/flag.jpg');
figure; imshow(imcomplement(f)); impixelinfo;

%c1=zeros(70,3);
%c1(:,1) = 0.0;
%c1(:,2) = 0.0;
%c1(:,3) = 0.0; % Blue
%c2=zeros(130,3);
%c2(:,1) = 0.8; % Red
%c2(:,2) = 0.0; % Blue
%c2(:,3) = 0.0; % Green
%c3=ones(55,3); % White
%c3(:,1) = 0.8; % Red
%c3(:,2) = 0.8; % Green
%c3(:,3) = 0.0; % Blue
%C = [   c1;
%        c2;
%        c3  ];
%figure;
%imshow(f, C)

f = imread('S5-Color/Cell2.jpg');
figure; imshow(f); impixelinfo;
k = 5;
u = unique(f);
[ idx, c ] = kmeans(u, 5);
c = sort(uint8(c));

CO = [];
prev = 0;
i = 1:k;
colors = rand(5,3);
for ind = i
    if ind == k
        s = 255 - prev;
    else
        s = c(ind)-prev;
    end
    
    r = 1:s;
    c_curr = zeros(s, 3);
    c_curr(r,:) = colors(ind);
    prev = c(ind);
    CO = [CO; c_curr];
end
disp(size(CO));

figure;
imshow(f, C)

% Matlab colormaps
f = imread('S5-Color/man.jpg');
figure; imshow(f); impixelinfo;
C = colorcube(256);
imshow(f, C);

% Full Color
f = imread('S5-Color/fresas.jpg');
figure; imshow(f); impixelinfo;
R = f(:,:,1);
G = f(:,:,2);
B = f(:,:,3);

subplot(2,2,1);
imshow(f);
title('Original');
subplot(2,2,2);
imshow(R);
title('R');
subplot(2,2,3);
imshow(G);
title('G');
subplot(2,2,4);
imshow(B);
title('B');

Rc = imcomplement(R); % Generates compement (negative) from red band.
f(:,:,1)=Rc;
figure, imshow(f);

Gc = im2double(G.^0.5); % Gamma filter to lighten up greens
f(:,:,2)=Gc;
figure, imshow(f, []);


% Exam Practice
addpath('/func/imnoise2.m');
addpath('/util/auto_adjust.m');

f = imread('S5-Color/fresas.jpg');
figure; imshow(f); impixelinfo;
R = f(:,:,1);
G = f(:,:,2);
B = f(:,:,3);

% Gaussian in Red
[ M, N ] = size(R);
n = imnoise2('gaussian', M, N, 0.5, 0.1);
Rc = auto_adjust(im2double(R) + n);
%imshow(Rc);

% Salt & Pepper on Blue
[ M, N ] = size(B);
n = imnoise2('salt & pepper', M, N, 0.5, 0.5);
Bc = auto_adjust(im2double(B) + n);

% Result Image
f(:,:,1) = im2uint8(Rc);
f(:,:,3) = im2uint8(Bc);
figure; imshow(f);

% A last one
addpath('/func/imnoise2.m');
addpath('/util/auto_adjust.m');

f = imread('S5-Color/fresas.jpg');
figure; imshow(f); impixelinfo;
R = im2double(f(:,:,1));
G = im2double(f(:,:,2));
B = im2double(f(:,:,3));

% Mask
mask = [1 1 1; 1 -8 1; 1 1 1];

% Gaussian filter to make a higher contrast
f(:,:,1) = im2uint8(R - imfilter(R, mask, 'replicate'));
f(:,:,2) = im2uint8(G - imfilter(G, mask, 'replicate'));
f(:,:,3) = im2uint8(B - imfilter(B, mask, 'replicate'));
figure, imshow(f);

%%% Color Spaces
rgb_image = imread('S5-Color/man.jpg');
cmy_image = imcomplement(rgb_image);
disp(rgb_image);
disp(cmy_image);
imshow(cmy_image);


%%% RGB to Indexed Image

% Description

RGB = imread('S5-Color/iris.tif');
%[X, map] = rgb2ind(RGB, COLORS_QTY, 'nodither');
%[Y, map2] = rgb2ind(RGB, COLORS_QTY, 'dither');

COLOR_INC = 16;
COLOR_TARGET = 256;
colors = COLOR_TARGET:-COLOR_INC:1;
disp(colors);

rows = floor(sqrt(COLOR_TARGET/COLOR_INC));
cols = floor((COLOR_TARGET/COLOR_INC)/rows);
cont = 1;
figure;
for i=colors
    [X, map] = rgb2ind(RGB, i, 'nodither');
    subplot(rows,cols,cont);
    imshow(X, map);
    title(['Total Colors: ', num2str(i)]);
    cont = cont + 1;
end

disp(size(map)); % map is COLORS_QTY x 3 (RGB), the index represents the color given to each pixel.





