% 1.- Get image's negative
                % aqu?? se define
                % el rango de lo
                % que interesa
f = imread('S2-Spatial/breast.tif');
figure, imshow(f), impixelinfo;
g = imadjust(f, [92/255;235/255], [0 ; 1], 1);
figure, imshow(g);

% 2.- Get image's logarithmic negative
f = im2double(imread('S2-Spatial/handRG.jpg'));
%s = log(1 + f);
%figure; imshow(f);
%figure; imshow(s);

figure;
for c=1:0.05:3
    s = c * log(1+f);
    title(c);
    imshow(s);
    pause(0.5);
end

% 3.- Get image's Power Transformation
    % uses rule r = c * e-^(r-1)
f = im2double(imread('S2-Spatial/handRG.jpg'));
figure;
for c=0.0:0.01:1
    s = c * exp(f-1);
    imshow(s);
    title(c);
    %pause(0.016);
end

% 4.- Gamma Function
    % uses rule r = c * r^(gamma)
    % gamma >1 darkens
    % gamma 1> lightens
f = im2double(imread('S2-Spatial/aerial.tif'));
figure; imshow(f);
figure;
gam = 2;
for c=0.0:0.01:1
    s = f.^gam;
    imshow(s);
    title(c);
    pause(0.01);
end

% 5.- Histogram, statistical metric for an image's values
f = imread('S2-Spatial/pollen.tif');
g = histeq(f);          % equalizes the histogram, generating a higher contrast
                        % on low-contrast images, but a low-contrast on high-contrast images
                        % moon.tif & pollen.tif. Uses Gaussian Equalization.
subplot(2,2,1);
imshow(f);
subplot(2,2,2);
imhist(f);
subplot(2,2,3);
imshow(g);
subplot(2,2,4);
imhist(g);

% 5.1.- Two mode Gauss, creates and aproximates image to two gaussian functions
addpath('func/twomodegauss.m');
f = imread('S2-Spatial/pollen.tif');
gauss = twomodegauss(5/255, 0.05, 200/255, 0.05, 0, 1, 0);
g = histeq(f, gauss);       % equalizes function using gauss-gaussian parameters
subplot(2,2,1), imshow(f);
subplot(2,2,2), imhist(f);
subplot(2,2,3), imshow(g);
subplot(2,2,4), imhist(g);

% 6.- Arithmetic Operations
f = imread('S2-Spatial/pollen.tif');

[M, N] = size(f);
g = 50 * ones(M, N);        % filter that clears an image
z = uint8(double(f) + g);   % value conversions for operability

% Plots
subplot(1,2,1), imshow(f);
subplot(1,2,2), imshow(z);

% 6.1 Sum of images
f = im2double(imread('S2-Spatial/tool1.gif'));
g = im2double(imread('S2-Spatial/tool2.gif'));

z = f - g;

subplot(1,3,1), imshow(f);
subplot(1,3,2), imshow(g);
subplot(1,3,3), imshow(z);

% 7.- Hadamarth Product (Element-wise product)( .* operator)
    % Selects sections of an image given a mask
f = im2double(imread('S2-Spatial/ckt.tif'));
g = im2double(imread('S2-Spatial/cktmask.tif'));

z = f .* g;
subplot(1,3,1), imshow(f);
subplot(1,3,2), imshow(g);
subplot(1,3,3), imshow(z);

% 8.- Matrix of M x M to create convolution
    % The setup of this code creates a blurry Image 
    % from source.
    M = 15;
    f = im2double(imread('S2-Spatial/testpattern1.tif'));
    mask = ones(M, M)/(M*M);
    g = imfilter(f, mask,'replicate');  % 'circular' gives interesting results
 
    subplot(1,2,1), imshow(f);
    subplot(1,2,2), imshow(g,[]);   % normalizes values to [0,1]

