addpath('func/paddedsize.m');
addpath('func/lpfilter.m');
addpath('func/dftuv.m');

%clear all
%close all
%clc

f=imread('data/s3_freq/bld.tif');
PQ=paddedsize(size(f)); %padding
F=fft2(f,PQ(1),PQ(2)); % transformada
Fc=fftshift(F); %centrar f

S=log(1+abs(Fc)); % espectro + filtro log 

%filter
H=lpfilter('ideal', PQ(1),PQ(2), 100);
G=F.*H;

g=real(ifft2(G));
[M N]=size(f);
g=g(1:M, 1:N);
F=fft2(g); % transformada
Fc=fftshift(F); %centrar f
Sg=log(1+abs(Fc)); % espectro + filtro log 

M1 = -M/2:1:M/2;
N1 = -N/2:1:N/2;

subplot(2,2,1)
imshow(f);
title('Original')

subplot(2,2,2)
% imshow(S,[])
imagesc(M1,N1,S)
title('Spectro original')
colormap('gray');

subplot(2,2,3)
imshow(g,[])
title('Filtrada')

subplot(2,2,4)
% imshow(Sg,[])
imagesc(M1,N1,Sg)
title('Spectro filtrada')
colormap('gray');