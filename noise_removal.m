addpath('/func/histroi.m');
addpath('/func/spfilt.m');
addpath('func/adpmedian.m');
% Identification of PDF Noise
f = imread('S4-Restauration/noisy_image7.jpg');
figure, imshow(f);

% Noise Identification
% Important to select polygon within
% uniform image section
%[ B, c, r ] = roipoly(f);
%figure, imshow(B);
%[ p, ~ ] = histroi(f, c, r);
%figure, bar(p);

% Smoothens noise, cleaning gaussian or uniform noise
% noisy_image3.jpg - Gaussian
a = spfilt(f, 'amean', 5, 5); % arithmetic mean
figure, imshow(a), title('Arithmetic Mean');

g = spfilt(f, 'gmean', 5, 5); % geometric mean
figure, imshow(g), title('Geometric Mean');

% For salt & pepper, but only one at a time
% noisy_image4.jpg - Salt & Pepper
g = spfilt(f, 'chmean', 3, 3, 1); % counter harmonic filter
figure, imshow(g), title('Counter-Harmonic Filter');

g = spfilt(f, 'median', 5, 5); % harmonic filter removes S&P
figure, imshow(g), title('Harmonic Filter');

% For salt & pepper, but only one at a time
% noisy_image5.jpg - Salt & Pepper
g = spfilt(f, 'max', 3, 3); % finds brightest spot in image (removes pepper)
figure, imshow(g), title('Counter-Harmonic (Max) Filter');

g = spfilt(f, 'min', 3, 3); % finds darkest spot in image (removes saltsalt)
figure, imshow(g), title('Counter-Harmonic (Max) Filter');

% Gauss + Uniform
% noise_image6.jpg
g = spfilt(f, 'midpoint', 3, 3); % Gauss + Uniform
figure, imshow(g), title('Midpoint Gauss + Uniform Filter');

% Combination of different noises
% noise_image7.jpg - Gaussian + Salt & Pepper
g = spfilt(f, 'atrimmed', 5, 5, 22); 
figure, imshow(g), title('Alpha Trimming Filter');

% Adaptative Filter
% Any image
g = adpmedian(f, 3);    %Universal, compares mean and variance of a small section
                        % of an image with the same parameters of the complete image
figure, imshow(g), title('Adaptative Median Filter');

% Notch Filter
addpath('util/get_filter_luis.m');
addpath('func/fft2.m');
addpath('func/notch.m');

img = im2double(imread('S4-Restauration/noisy_image1.jpg'));

PQ = paddedsize(size(img)); % Padding
F = fft2(img, PQ(1), PQ(2)); % Fourier transform
Fc = fftshift(F); % Centers F

S = log(1+abs(Fc)); % espectro + filtro log 
imagesc(S);


% Filter Construction
H = 1 - notch(PQ, 2, 0.5, 300); % Notch filter (PQ, Direction, Distance from center, Radius)

G = F.*H; % Has desired H


g = real(ifft2(G));
[ M, N ] = size(img);
g = g(1:M, 1:N);
F=fft2(g); % transformada
Fc=fftshift(F); %centrar f
Sg=log(1+abs(Fc)); % espectro + filtro log 


M1 = -M/2:M/2;
N1 = -N/2:N/2;

% Plotting
subplot(2, 2, 1);
imshow(img);
title('Original');

subplot(2, 2, 2);
imagesc(M1, N1, S);
title('Original Spectre')
colorbar
%colormap('gray');

subplot(2, 2, 3);
imshow(g, []);
title('Filtered')

subplot(2, 2, 4);
imagesc(M1, N1, Sg);
title('Filtered Spectrum')
colorbar
