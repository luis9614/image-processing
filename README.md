# Image Processing - Fall 2018

Class Notes & Code authored by **Luis Enrique Correa Morán**  

**ID**  
> 0186950  

**e-mail**  
> luiscorrea9614@gmail.com  

Professor: M.A. Ramiro Velázquez  

Artificial Intelligence Engineering 2018  

## Topics

### First Term

#### 1. Fundamentals  

* [warmup.m](warmup.m)

#### 2. Spatial  

* [spatial.m](spatial.m)

#### 3. Frequencies  

* [freq.m](freq.m)
* [ej_freq.m](ej_freq.m)

### Second Term

#### 4. Restauration  

* [noise.m](noise.m)  
* [noise_identification.m](noise_identification.m)
* [noise_removal.m](noise_removal.m)
* [movement_noise.m](movement_noise.m)

#### 5. Color [(See documentation)](Color.md)  

* [color.m](color.m)

#### 6. Morphology [(See documentation)](Morphology.md)  

* [morphology.m](morphology.m)

## Folders

### func/

Set of Matlab Community open source libraries and functions. Requires for the scripts contained in the project.

### util/

Set of functions and libraries created by @luis9614.

## Notes

Add the **util/** and **func/** folders to your Matlab project % path. In file explorer right-click the aforementioned folders and select

> Add to path -> Selected Folders & Subfolders
