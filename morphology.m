folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

% Image Filling

% Image A
A = [   0 0 0 0 0 0 0;
        0 0 1 1 0 0 0;
        0 1 0 0 1 0 0;
        0 0 1 0 1 0 0;
        0 0 1 0 1 0 0;
        0 1 0 0 0 1 0;
        0 1 0 0 0 1 0;
        0 1 1 1 1 0 0;
        0 0 0 0 0 0 0;
];

figure;
subplot(1, 2, 1);
imagesc(A);
colormap('gray');

% Structural Element
B = strel('diamond', 1);

% The image that will be modified on each step
Xk = zeros(size(img));

% Starting Point
Xk(3, 3) = 1;

% Algorithm
flag = 0;
while flag == 0
    subplot(1, 2, 2);
    imagesc(or(Xn, A));
    pause(0.4);
    Xn = imdilate(Xk, B)&~A;
    if(Xn-Xk)==0
        flag=1;
    else
        Xk=Xn;
    end
end

% Labelling
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

f = imread('data/S6-Morphology/five-objects.tif');
figure;
imagesc(f);

[L, num] = bwlabel(f, 8); % (file, connectivity)
figure;
fig = 1:1:num;
for i=fig
    subplot(1, num, i);
    imagesc(L==i);
    colormap('gray');
end

% Reconstruction
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path
% Objective, to keep the letters that have a 'palito'
% Choose a structural pattern that fits the features you want to keep. In this case, a line.
A = imread('data/S6-Morphology/book-text.tif');
figure;
subplot(2,2,1);
imshow(A);
title('Original');

B = strel('line', 50, 90);
C1 = imerode(A, B);
subplot(2,2,2);
imshow(C1);
title('Eroded Image');

subplot(2,2,3);
C2 = imopen(A, B);
imshow(C2);
title('Opened Image');

R = imreconstruct(C1, A);
subplot(2,2,4);
imshow(R);
title('Reconstructed');

% Exercise
% Objective: Leave only circles from shapes2.tif
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

A = imread('data/S6-Morphology/shapes2.tif');
B = strel('disk', 18);
C = imerode(A, B); % Erode image. Only points are left.

R = imreconstruct(C, A); % Reconstructed image containing the desired feature.
figure;
imshow(R);

% Reconstruction in Non-Binary Images
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

A = imread('data/S6-Morphology/aerial.tif');
B = strel('square', 5);
C = imdilate(A, B);
D = imerode(A, B);
figure;
subplot(2,2,1);
imshow(A);
title('Original')
subplot(2,2,2);
imshow(C);
title('Dilation')
subplot(2,2,3);
imshow(D);
title('Erosion')

E = C - D; % Dilation - Erosion returns contours
subplot(2,2,4);
imshow(E);
title('Dilation-Erosion=Contours')

% Top Hat
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

A = imread('data/S6-Morphology/rice.tif');
B = strel('disk', 10);
C = imtophat(A, B);


figure;
subplot(1,2,1);
imshow(A);
title('Original Image');
subplot(1,2,2);
imshow(C);
title('Uniform Image');

% Binarization
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

A = imread('data/S6-Morphology/aerial.tif');
T = graythresh(A);
C = imadjust(A, [T, T+1e-4], [0, 1]);
figure;
subplot(1,2,1);
imshow(A);
title('Original Image');
subplot(1,2,2);
imshow(C);
title('Binarized Image');

% Binarization Exercise
folder = fileparts(which(mfilename));
addpath(genpath(folder)); % adds folder + all subfolders in path

% Binarizing of Image
A = imread('data/S6-Morphology/calculator.tif');
figure;
subplot(2,2,1);
imshow(A);
title('Original Image');

T = graythresh(A);
C = imadjust(A, [T, T+1e-4], [0, 1]);

subplot(2,2,2);
imshow(C);
title('Binarized Image');

% Structural Element for removing horizontal keyboard lines.
B = strel('line', 35, 0);
D = imerode(C, B); % Get horizontal lines by eroding.
R = imreconstruct(D, C); % Reconstruct horizontal lines.
RES = C - R; % Remove horizontal lines from original.

subplot(2,2,3);
imshow(RES);
title('Remove Horizontal Lines');

% Structural Element for removing vertical keyboard lines.
B = strel('line', 4, 0);
D = imerode(RES, B); % Get everything but vertical lines
R = imreconstruct(D, RES); % Reconstruct image

subplot(2,2,4);
imshow(R);
title('Remove Vertical Lines (Final)');

% Get the eyes from face.jpg
A = imread('data/S6-Morphology/face.jpg');
figure;
imshow(A);
A = rgb2gray(A);


T = 0.35; % T = graythresh(A);
J = imadjust(A, [T; T+1e-4], [1; 0]);
J = imfill(J, 'holes');
figure;
imshow(J); % Filled binarized image

B = strel('disk', 30); 
C = imerode(J, B); % DELETES EYES AND LEAVES HAIR
figure;
imshow(C);
title('LOL');

D = imreconstruct(C, J);
E = imsubtract(J,D);
imshow(D);

% Full image without hair
% Erode and dilate with the same Structural Element
B = strel('disk', 20);
C = imerode(D, B);
figure, imshow(C); 