f = imread('S4-Restauration/turbulence1.jpg');
f = im2double(f);
imshow(f);

H = fspecial('motion', 50, 55);  % Creates movement filter
g = imfilter(f, H, 'circular'); % Apllies filter to image

figure, imshow(g);

% H = n = 0
% g = fH

% Weiner Filter
% Brute force to find X
for x=5:5:90
    for th=5:10:90
        H = fspecial('motion', x, th);  % Creates movement filter
        fe = deconvwnr(g, H);
        imshow(fe, []), title(sprintf('x=%d and th=%d',x, th));
        pause(0.1);
    end
end