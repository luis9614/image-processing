clear all; % clears memory
close all;
clc;

% Main Program
f = imread('S2-Spatial/grayscale.tif');

%   pixel values    distance between pixels
%                   (euclidean)
%   impixelinfo;    imdistline;

[M, N]= size(f);

imshow(f);
impixelinfo; %displays pixel values

g = [   86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191
        86    99   113   126   138   148   159   169   180   191    ];
g = uint8(g);
disp('Mats');
imwrite(g, 'test.jpg');

% Matrix
mat = eye(7);
disp(mat);
mat(5,5)

% Vectors
i = 2;
vect = 2:3*i:100;
disp(vect);