clear all
close all
clc

f=im2double(imread('BilleteOK.tif'));
g=im2double(imread('Billete2.tif'));

title('Original');

subplot(1,3,1);
imshow(f);
title('Original');

subplot(1,3,2);
imshow(g);
title('Imagen a analizar');


comp = abs(f-g);
avgError = mean2(comp);
A = avgError 

subplot(1,3,3);
imshow(1 - comp);

if avgError > 0
    title('Rechazar');
else
    title('Aceptar');
end





