file_ok = 'P3/BilleteOk.tif';
file_co = 'P3/Billete2.tif';


img_ok = im2double(imread(file_ok));
img_co = im2double(imread(file_co));

comparison = img_ok - img_co;
%comparison = auto_adjust(comparison);
tag = 'Hi';
un = unique(comparison);
disp('Unique');
disp(un);

[M, N] = size(un);

if M > 1
    tag='Fake Bill';
else
    tag='Accept Bill';
end

figure
subplot(1, 3, 1);
imshow(img_ok);
title('Original');

subplot(1, 3, 2);
imshow(img_co);
title('Comparing...');

subplot(1, 3, 3);
imshow(comparison,[]);
title(tag);