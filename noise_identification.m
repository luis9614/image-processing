addpath('/func/histroi.m');

% Identification of PDF Noise
f = imread('S4-Restauration/noisy_image2.tif');
imshow(f);

[ B, c, r ] = roipoly(f); % Region Of Interest - draws poligont in free movement
imshow(B); % B = Region contained by the polygon
[ p, ~ ] = histroi(f, c, r); % generate region's histogram
% Only a uniform region is needed as it returns an accuarate noise
% a non-uniform region introduces weird frequencies to the histogram

figure, bar(p);

% Exercise
addpath('func/histroi.m');
% Im1 = gaussian
% Im2 = Rayeigh | Gamma
% Im3 = Gamma
% Im4 = Exponential
% Im5 = Uniform
% Im6 = Salt & Pepper
f = imread('S4-Restauration/Im6.jpg');
imshow(f);
[ B, c, r ] = roipoly(f); % Region Of Interest - draws poligont in free movement
imshow(B); % B = Region contained by the polygon
[ p, np ] = histroi(f, c, r); % generate region's histogram
% Only a uniform region is needed as it returns an accuarate noise
% a non-uniform region 
figure, bar(p);


f = imread('S4-Restauration/noisy_image1.tif');
imshow(f);
[ M, N ] = size(f);
f = f(1:M, 1:N);
F=fft2(g); % transformada
Fc=fftshift(F); %centrar f
Sg=log(1+abs(Fc)); % espectro + filtro log 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                    %
%                      Notes                         %
%                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Detection       Filter
%     |----- histroi ------- spatial
%  n -|
%     |----- Spectrum ------ frequential
%