# Color

Reference file [color.m](color.m)  
There's two types of color processing pseudo & full color.

## Pseudo Color

Color maps - map a grey value to a color, and color coding every pixel to its correxponding value.

Matlab has some color maps implemented.
![Matlab](images/matlab_colormaps.jpg)

```Matlab
f = imread('S5-Color/mouse.jpg');   % Reads image
figure; imshow(f); impixelinfo;     % Shows original image

C = jet(256);                       % # of divisions on the color spectre is specified
imshow(f, C);                       % shows result image
```

[Matlab ColorMap Reference](https://la.mathworks.com/help/matlab/colors-1.html)

## Full Color

Processing each channel separately and joining them at the end.

### Separating an image into its components.

```Matlab
f = imread('S5-Color/fresas.jpg');
figure; imshow(f); impixelinfo;
R = f(:,:,1);
G = f(:,:,2);
B = f(:,:,3);
```

## Exam Practice

Add different noises to each band of an image.  

```Matlab
addpath('/func/imnoise2.m');
addpath('/util/auto_adjust.m');

f = imread('S5-Color/fresas.jpg');
figure; imshow(f); impixelinfo;
R = f(:,:,1);
G = f(:,:,2);
B = f(:,:,3);

% Gaussian in Red
[ M, N ] = size(R);
n = imnoise2('gaussian', M, N, 0.5, 0.1);
Rc = auto_adjust(im2double(R) + n);
%imshow(Rc);

% Salt & Pepper on Blue
[ M, N ] = size(B);
n = imnoise2('salt & pepper', M, N, 0.5, 0.5);
Bc = auto_adjust(im2double(B) + n);

% Result Image
f(:,:,1) = im2uint8(Rc);
f(:,:,3) = im2uint8(Bc);
figure; imshow(f);
```

**Note: To perform a noise evaluation, each layer should be separated and processed separately. Noises of each layer should be corrected and at last the image must be joined.**

## Laplacian Filter

```Matlab
addpath('/func/imnoise2.m');
addpath('/util/auto_adjust.m');

f = imread('S5-Color/fresas.jpg');
figure; imshow(f); impixelinfo;
R = im2double(f(:,:,1));
G = im2double(f(:,:,2));
B = im2double(f(:,:,3));

% Mask
mask = [1 1 1; 1 -8 1; 1 1 1];

% Gaussian filter to make a higher contrast
f(:,:,1) = im2uint8(R - imfilter(R, mask, 'replicate'));
f(:,:,2) = im2uint8(G - imfilter(G, mask, 'replicate'));
f(:,:,3) = im2uint8(B - imfilter(B, mask, 'replicate'));
figure, imshow(f);
```