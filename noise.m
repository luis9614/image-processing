addpath('/func/imnoise2.m');
addpath('/func/imnoise3.m');
addpath('/util/auto_adjust.m');
% Fourth topic

% Probabilistic Density Function (PDF) Noise

% f -> original image
% g -> degradation fenomena
% n -> noise
% g = f + n

f = im2double(imread('S4-Restauration/Patt1.jpg'));
[ M, N ] = size(f);
imshow(f);

i_mean = 0.1;
i_sd = 0.2;


% imnoise2 filters [gaussian, rayleigh]
%n = imnoise2('gaussian', M, N, 0.5, 0.1);
%n = imnoise2('rayleigh', M, N, 0, 1);
%n = imnoise2('erlang', M, N, 7, 2);
%n = imnoise2('exponential', M, N, 5);
%n = imnoise2('uniform', M, N, 0.25, 0.75);
n = imnoise2('salt & pepper', M, N, 0.5, 0.5);
figure, imshow(n)

g = f + n;
figure, imshow(g, []);


% Frequential Noise
addpath('imnoise2.m');
addpath('imnoise3.m');
addpath('autoadjust.m');

f = im2double(imread('S4-Restauration/Patt1.jpg'));
[ M, N ] = size(f);

C = [0 5; 5 0];
[n ,R, S] = imnoise3(M, N, C);
% n -> spatial pattern
% R -> Fourier transform
% S -> Spectrum

n = auto_adjust(n); % gets values into a [ 0, 1 ] range

g = f + n;

figure
subplot(1,3,1);
imshow(n, []);

subplot(1,3,2);
imshow(S, []);

subplot(1,3,3);
imshow(g, []);
