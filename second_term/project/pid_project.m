function varargout = pid_project(varargin)
% PID_PROJECT MATLAB code for pid_project.fig
%      PID_PROJECT, by itself, creates a new PID_PROJECT or raises the existing
%      singleton*.
%
%      H = PID_PROJECT returns the handle to a new PID_PROJECT or the handle to
%      the existing singleton*.
%
%      PID_PROJECT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PID_PROJECT.M with the given input arguments.
%
%      PID_PROJECT('Property','Value',...) creates a new PID_PROJECT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pid_project_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pid_project_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help pid_project

% Last Modified by GUIDE v2.5 05-Nov-2018 17:05:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pid_project_OpeningFcn, ...
                   'gui_OutputFcn',  @pid_project_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before pid_project is made visible.
function pid_project_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pid_project (see VARARGIN)

% Choose default command line output for pid_project
handles.output = hObject;

handles.current_filter = 0;

handles.cam = videoinput('macvideo', 1, 'YCbCr422_1280x720');
set(handles.cam, 'ReturnedColorSpace', 'RGB');
set(handles.cam,'FramesPerTrigger',1);
% Go on forever until stopped
set(handles.cam,'TriggerRepeat',Inf);
triggerconfig(handles.cam,'manual');

handles.cam.FramesPerTrigger = 1;
% output would image in RGB color space
handles.cam.ReturnedColorspace = 'rgb';
% we need this to know the image height and width
vidRes = get(handles.cam, 'VideoResolution');
% image width
imWidth = vidRes(1);
% image height
imHeight = vidRes(2);
% number of bands of our image (should be 3 because it's RGB)
nBands = get(handles.cam, 'NumberOfBands');
% create an empty image container and show it on axPreview
hImage = image(zeros(imHeight, imWidth, nBands), 'parent', handles.imDisp);
% begin the webcam preview
preview(handles.cam, hImage);

setGlobalFilter(0);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pid_project wait for user response (see UIRESUME)
% uiwait(handles.pid_project);


% --- Outputs from this function are returned to the command line.
function varargout = pid_project_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.output = hObject;
GetImage(handles);
% Get default command line output from handles structure

function GetImage(handles)
    camera = webcam;
    axes(handles.imDisp);
    while(true)
        img = snapshot(camera);
        switch getGlobalFilter
            case 1 % Negative
                img = imcomplement(img);
                imshow(img);
            case 2 % Color Reduction
                val = double(uint8(handles.sliderColors.Value));
                handles.textColor.String = val;
                [img, map] = rgb2ind(img, val, 'nodither');
                imshow(img, map);
            case 3 % Gaussian Noise
                val = handles.sliderGauss.Value;
                handles.textGauss.String = num2str(val);
                img = uint8(imnoise(img, 'gaussian', val, 0.05));
                imshow(img);
            case 4 % Hemianopia
                val = handles.sliderHem.Value;
                handles.textHem.String = val;
                g = imgaussfilt(img, val);
                [M, N, ~] = size(img);
                n = N/2;
                %m = M/2;
                mix = [g(1:M,1:n,:) img(1:M,n:N,:)]; % integrate both image's halves
                imshow(mix,[]);
            case 5 % Spectrum
                PQ=paddedsize(size(img)); %padding
                F=fft2(img,PQ(1),PQ(2)); % transformada
                Fc=fftshift(F); %centrar f
                S=log(1+abs(Fc)); % espectro + filtro log 
                imshow(S);
            case 6 % Grid
                [ M, N, ~ ] = size(img); %get original dimensions
                n_cols = 1:2:M;
                n_rows = 1:2:N;
                img = img(n_cols, n_rows, :);
                res = [img, img; img, img ];
                imshow(res);
            case 7 % Gamma Filter
                val = uint8(handles.sliderGamma.Value);
                if val==0
                    val = 1;
                end
                handles.textGamma.String = val;
                img = img.^val;
                imshow(img,[]);
            case 8 % Rotation
                img = imrotate(img, 90);
                imshow(img);
            case 9 % Periodic Noise
                img = im2double(img);
                [ M, N,  ~] = size(img);
                C = [0 5; 5 0];
                [n, ~, ~] = imnoise3(M, N, C);
                n = auto_adjust(n);
                noise = repmat(n, 1, 1, 3);
                imshow(img + noise, []);
            case 10
                C = hot(256);
                img = rgb2gray(img);
                %img = ind2rgb(img, C);
                imshow(img, C);

            otherwise % Original
                imshow(img);
        end
    end
    


%hImage = image(zeros(imHeight, imWidth, nBands), 'parent', handles.imDisp);
%preview(handles.cam, img);

% --- Executes on button press in btnNegative.
function btnNegative_Callback(hObject, eventdata, handles)
% hObject    handle to btnNegative (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(1);

% --- Executes on button press in btnGrid.
function btnGrid_Callback(hObject, eventdata, handles)
% hObject    handle to btnGrid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(6);

% --- Executes on button press in btnOriginal.
function btnOriginal_Callback(hObject, eventdata, handles)
% hObject    handle to btnOriginal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(0);


% --- Executes on button press in btnColorReduction.
function btnColorReduction_Callback(hObject, eventdata, handles)
% hObject    handle to btnColorReduction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(2);

function editColores_Callback(hObject, eventdata, handles)
% hObject    handle to editColores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editColores as text
%        str2double(get(hObject,'String')) returns contents of editColores as a double
setGlobalCR(str2double(get(hObject,'String')));


% --- Executes during object creation, after setting all properties.
function editColores_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editColores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function setGlobalCR(val)
global colors_qty
colors_qty = val;

function setGlobalFilter(val)
global myfilter
myfilter = val;

function r = getGlobalFilter
global myfilter
r = myfilter;


% --- Executes on button press in btnGaussianNoise.
function btnGaussianNoise_Callback(hObject, eventdata, handles)
% hObject    handle to btnGaussianNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(3);


function editGaussianNoise_Callback(hObject, eventdata, handles)
% hObject    handle to editGaussianNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editGaussianNoise as text
%        str2double(get(hObject,'String')) returns contents of editGaussianNoise as a double


% --- Executes during object creation, after setting all properties.
function editGaussianNoise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editGaussianNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnHemianopia.
function btnHemianopia_Callback(hObject, eventdata, handles)
% hObject    handle to btnHemianopia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(4);


% --- Executes on button press in btnSpectrum.
function btnSpectrum_Callback(hObject, eventdata, handles)
% hObject    handle to btnSpectrum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(5);


% --- Executes on button press in btnGamma.
function btnGamma_Callback(hObject, eventdata, handles)
% hObject    handle to btnGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(7);


% --- Executes on button press in btnRotate.
function btnRotate_Callback(hObject, eventdata, handles)
% hObject    handle to btnRotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(8);


% --- Executes on button press in btnPeriodic.
function btnPeriodic_Callback(hObject, eventdata, handles)
% hObject    handle to btnPeriodic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(9);

% --- Executes on slider movement.
function sliderColors_Callback(hObject, eventdata, handles)
% hObject    handle to sliderColors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% --- Executes during object creation, after setting all properties.
function sliderColors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderColors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderGauss_Callback(hObject, eventdata, handles)
% hObject    handle to sliderGauss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderGauss_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderGauss (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderHem_Callback(hObject, eventdata, handles)
% hObject    handle to sliderHem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderHem_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderHem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderGamma_Callback(hObject, eventdata, handles)
% hObject    handle to sliderGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderGamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in btnColorMap.
function btnColorMap_Callback(hObject, eventdata, handles)
% hObject    handle to btnColorMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
setGlobalFilter(10);