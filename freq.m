addpath('/func/paddedsize.m');
addpath('/func/lpfilter.m');
addpath('/func/dftuv.m');
addpath('/util/get_filter_luis.m');

% La transporfada de Fourier detecta frecuencias
% El filtro LP las filtra (elimina)

f = imread('s3_freq/bld.tif');
PQ = paddedsize(size(f)); % Padding
disp(size(f));
disp(size(PQ));
F = fft2(f, PQ(1), PQ(2)); %Transform Fourier
Fc = fftshift(F); % Centers F

S = log(1+abs(Fc)); % espectro + filtro log 
imagesc(S);
% Filter Construction
    
%H = get_filter_luis('LP', 'ideal', PQ(1), PQ(2), 100); % Low Pass Filter
%H = get_filter_luis('HP', 'ideal', PQ(1), PQ(2), 200); % High Pass Filter
H = get_filter_luis('BP', 'ideal', PQ(1), PQ(2), 100, 200); % Band Pass
%H = get_filter_luis('BR', 'ideal', PQ(1), PQ(2), 50, 100); % Band Rejection

G = F.*H; % Has desired H


g = real(ifft2(G));
[ M, N ] = size(f);
g = g(1:M, 1:N);
F=fft2(g); % transformada
Fc=fftshift(F); %centrar f
Sg=log(1+abs(Fc)); % espectro + filtro log 


M1 = -M/2:M/2;
N1 = -N/2:N/2;

% Plotting

subplot(2, 2, 1);
imshow(f);
title('Original');

subplot(2, 2, 2);
imagesc(M1,N1, S);
title('Original Spectre')
colorbar
%colormap('gray');

subplot(2, 2, 3);
imshow(g, []);
title('Filtered')

subplot(2, 2, 4);
imagesc(M1, N1, Sg);
title('Filtered Spectrum')
colorbar
%colormap('gray');



%eXPLICACI??N DE TRANSFORMADA DE FOURIER, detecci??n de patrones frecuenciales
%f = imread('s3_freq/patt2.tif');
%F = fft2(f, PQ(1), PQ(2)); % Fourier Transform
%Spectrum = abs(F) % Removes Imaginary values